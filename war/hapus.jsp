<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Hapus</title>
    </head>
    <body>
        <h1>Apakah di hapus!</h1>
        <form action="/hapus" method="post"> 
            <input type="hidden" name="hdnId" value="${data.key.id}">
            Nama  : <input type="text" name="txtNama" value="${data.properties.Nama}" disabled><br/>
            N I M : <input type="text" name="txtNim" value="${data.properties.Nim}" disabled><br/>
            Email : <input type="text" name="txtEmail" value="${data.properties.Email}" disabled><br/>
            NoHP  : <input type="text" name="txtNoHP" value="${data.properties.NoHP}" disabled><br/>
            Status : <select name="Aktif">
            	<option value="1">Aktif</option>
            	<option value="0">Tidak aktif</option>
           	 </select>
            <input type="submit" value="Hapus">
        </form>
    </body>
</html>

