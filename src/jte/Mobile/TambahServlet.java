package jte.Mobile;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

@SuppressWarnings("serial")
public class TambahServlet extends HttpServlet 
{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException 
	{
		resp.setContentType("text/html");
		RequestDispatcher jsp = req.getRequestDispatcher("tambah.jsp");
		jsp.forward(req, resp);		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException
	{
		String Nama = req.getParameter("txtNama");
		String Nim = req.getParameter("txtNim");
		String Email = req.getParameter("txtEmail");
		String NoHP = req.getParameter("txtNoHP");
		Boolean Aktif = Boolean.valueOf(req.getParameter("Aktif"));

		Entity entity = new Entity("DaftarMahasiswa");
		entity.setProperty("Nama", Nama);
		entity.setProperty("Nim", Nim);	
		entity.setProperty("Email", Email);
		entity.setProperty("NoHP", NoHP);
		entity.setProperty("Aktif", Aktif);
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		datastoreService.put(entity);
		
		resp.sendRedirect("/");	
	}
}
